FROM centos

RUN yum install https://centos7.iuscommunity.org/ius-release.rpm -y
RUN yum install python36u python36u-pip python36u-devel -y

ADD legate.py legate.py
ADD wsgi.py wsgi.py
ADD requirements.txt requirements.txt

RUN ln -s /bin/pip3.6 /bin/pip
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 5000

CMD python3.6 wsgi.py
