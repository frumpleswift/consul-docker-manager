This project uses consul key/value configurations to manager docker containers.

This assumes consul and registrator are both running on a docker host.

https://gliderlabs.com/registrator/latest/user/quickstart/

docker network create grpc
docker run -d -p 8500:8500 --name=consul --net=grpc gliderlabs/consul-server -bootstrap
docker run -d     --name=registrator     --net=grpc     --volume=/var/run/docker.sock:/tmp/docker.sock     gliderlabs/registrator:latest       consul://consul:8500

API Examples

List Managed Services:

bash-4.2$ curl http://localhost:8080/api/get_managed_services
[
  "helloworld"
]

Get the Configuration for a Service:

bash-4.2$ curl http://localhost:8080/api/get_config?service=helloworld
{
  "env": [
    "DB_HOST=fake",
    "DB_PORT=FAKE"
  ],
  "image": "helloworld",
  "max-containers": 6,
  "min-containers": 3,
  "network": "grpc",
  "service": "helloworld"
}

Retrieve the Consul Endpoints for a Managed Service (optional parameter service=<<service_name>>):

bash-4.2$ curl http://localhost:8080/api/get_nodes
{
  "helloworld": [
    {
      "Address": "172.18.0.6",
      "CreateIndex": 0,
      "EnableTagOverride": false,
      "ID": "fccd8cea30b2:modest_visvesvaraya:50051",
      "ModifyIndex": 0,
      "Port": 9021,
      "Service": "helloworld",
      "Tags": null
    }
  ]
}

Remove a Managed Service:

bash-4.2$ curl http://localhost:8080/api/remove_managed_service?service=helloworld
{"helloworld":"removed"}


Create or Update a Managed Service:
bash-4.2$ curl -X POST -H "Content-Type:application/json" \
> -d '{"image":"helloworld","service":"helloworld","min-containers":5,"max-containers":8,"network":"grpc","env":["DB_HOST=fake","DB_PORT=FAKE"]}' \
> http://localhost:8080/api/add_managed_service
{
  "helloworld": "updated"
}

bash-4.2$ curl http://localhost:8080/api/apply_config?service=helloworld
{
  "helloworld": "applied"
}

[root@jon-computer ~]# docker ps
CONTAINER ID        IMAGE                           COMMAND                  CREATED             STATUS              PORTS                                                                                NAMES
091302399b5e        helloworld                      "/bin/sh -c 'pytho..."   25 seconds ago      Up 24 seconds       0.0.0.0:9027->50051/tcp                                                              determined_payne
b0dd52cc42f8        helloworld                      "/bin/sh -c 'pytho..."   25 seconds ago      Up 24 seconds       0.0.0.0:9026->50051/tcp                                                              sleepy_darwin
5718f0ae83c9        helloworld                      "/bin/sh -c 'pytho..."   26 seconds ago      Up 25 seconds       0.0.0.0:9025->50051/tcp                                                              sad_mirzakhani
5203c3d55922        helloworld                      "/bin/sh -c 'pytho..."   27 seconds ago      Up 26 seconds       0.0.0.0:9024->50051/tcp                                                              agitated_ardinghelli
d662527531fb        helloworld                      "/bin/sh -c 'pytho..."   58 minutes ago      Up 58 minutes       0.0.0.0:9021->50051/tcp                                                              modest_visvesvaraya

Managing from inside a container:

docker build -t manager .
