""" provides management of docker containers via consul key/value configurations """
import consul
import json
import docker
import time
import json
import os

LEGATE_HOST = os.environ["LEGATE_HOST"]

class Manager():
    """ docker manager class """
    def __init__(self,consul_host:str="localhost",consul_port:str="8500"):
        self.__consul_host=consul_host
        self.__consul_port=consul_port
        self.__consul = consul.Consul(host=consul_host,port=consul_port)
        self.__docker = docker.from_env()
        self.__nodes = {}

    def get_managed_services(self):
        """ return a list of all services managed in consul """
        index, managed_raw=self.__consul.kv.get("managed")
        if managed_raw is not None:
            return json.loads(managed_raw["Value"])
        return []

    def load_nodes(self):

        self.__nodes = {}
        managed = self.get_managed_services()
        for service in managed:
            self.__nodes[service]=[]
        nodes = self.__consul.agent.services()
        for key in nodes:
            if nodes[key]["Service"] in managed:
                self.__nodes[nodes[key]["Service"]].append(nodes[key])       
                
    def get_nodes(self,service=None):
        if service is not None:
            return self.__nodes[service]
        else:
            return self.__nodes

    def remove_managed_service(self,service):
        """
        will remove the service configuration from consul kv
        will also remove from the managed_service kv
        """
        managed=self.get_managed_services()
        if service in managed:
            managed.remove(service)
            managed_raw=json.dumps(managed)
            self.__consul.kv.put("managed",managed_raw)

        self.__consul.kv.delete(service) 

    def add_managed_service(self,service_config:dict):
        """
        will add/update the service to the consul kv managed entry
        will add/update the service_config to consul kv with name service
        dict format (required elements):
             {"image":"helloworld"
             ,"service":"helloworld"
             ,"targets":["worldhost"]
             ,"min-containers":3
             ,"max-containers":6
             ,"network":"grpc"
             ,"env":["DB_HOST=fake","DB_PORT=FAKE"] 
             }
        optional dict elements:
           If you want the service to run on only specific hosts:
           "host_affinity":["host1","host2"]
 
        """
        managed=self.get_managed_services()
        if service_config["service"] not in managed:
            managed.append(service_config["service"])
            managed_raw=json.dumps(managed)
            self.__consul.kv.put("managed",managed_raw)
 
        self.__consul.kv.put(service_config["service"],json.dumps(service_config))

    def get_config(self,service):
        
        index, service_raw = self.__consul.kv.get(service)
        return json.loads(service_raw["Value"])

    def apply_config(self,service):

        index,config_raw=self.__consul.kv.get(service)
        service_config = json.loads(config_raw["Value"])

        do_apply = True

        if "host_affinity" in service_config:
            if LEGATE_HOST in service_config["host_affinity"]:
                do_apply = True
            else:
                do_apply = False


        if do_apply:
            #remove containers down to max
            for rm in range (service_config["max-containers"],len(self.__nodes[service])):
                print("Removing container for {}".format(service),flush=True)
                #print( self.__docker.containers.list(filters={"status":"running","ancestor":service_config["image"]}),flush=True)
                self.__docker.containers.list(filters={"status":"running","ancestor":service_config["image"]})[0].stop()

            #add containers up to min
            for make in range(0,service_config["min-containers"]-len(self.__nodes[service])):
                print("Adding container for {}".format(service),flush=True)
                container = self.__docker.containers.run(image=service_config["image"]
                                                        ,auto_remove=True
                                                        ,detach=True
                                                        ,network=service_config["network"]
                                                        ,ports={"50051":None}
                                                        ,environment=service_config["env"]
                                                        )

if __name__=="__main__":

    service ={"image":"helloworld"
             ,"service":"helloworld"
             ,"min-containers":3
             ,"max-containers":6
             ,"network":"grpc"
             ,"env":["DB_HOST=fake","DB_PORT=FAKE"]
             }

    manager = Manager()

    manager.add_managed_service(service)

    manager.load_nodes()
    print(manager.get_nodes())

    print(manager.get_config("helloworld"))

    manager.apply_config("helloworld")

    manager.load_nodes()
    print(manager.get_nodes())


