""" flask api for managing the consul docker configurations """
from flask import Flask, flash, redirect, render_template, request, session, abort, url_for, jsonify
from legate import Manager
from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)

consul_host="consul"
manager = Manager(consul_host)

def poller():
    manager.load_nodes()
    for service in manager.get_managed_services():
        try:
            print("Applying configuration for {}".format(service),flush=True)
            manager.apply_config(service)
        except Exception as ex:
            print("Error applying configuration for {}: {}".format(service,ex),flush=True)    

@app.route("/api/get_config", methods=['GET'])
def get_config():
    service=request.values["service"]
    return jsonify(manager.get_config(service))

@app.route("/api/apply_config", methods=['GET','POST'])
def apply_config():
    #print(request.values)
    service=request.values["service"]
    manager.load_nodes()
    #print(manager.get_nodes())
    manager.apply_config(service)
    return jsonify({service:"applied"})

@app.route("/api/remove_managed_service", methods=['GET','POST'])
def remove_managed_service():
    service=request.values["service"]
    manager.remove_managed_service(service)
    return jsonify({service:"removed"})

@app.route("/api/get_managed_services", methods=['GET'])
def get_managed_services():
    return jsonify(manager.get_managed_services())

@app.route("/api/get_nodes", methods=['GET'])
def get_nodes():
    manager.load_nodes()
    service=None
    if "service" in request.values:
        service=request.values["service"]
    return jsonify(manager.get_nodes(service))

@app.route("/api/add_managed_service", methods=['POST'])
def add_managed_service():
    service_config=request.json
    manager.add_managed_service(service_config)
    return jsonify({service_config["service"]:"updated"})

if __name__ == "__main__":

    sched = BackgroundScheduler(daemon=True)
    sched.add_job(poller,'interval',minutes=1)
    sched.start()

    app.run(host='0.0.0.0', port=8080)
