#!/bin/bash
docker stop legate
docker rm legate
docker build -t legate .
docker run -p 8080:8080 -d -v /var/run/docker.sock:/var/run/docker.sock --name legate --network bridge0 -e LEGATE_HOST=$(hostname) legate
docker logs -f legate
